<?php

// connect to database when form is valid - check if local or live (on WebLeeSam.com)
$ip = $_SERVER['SERVER_ADDR'];

//echo $ip . "<br>";
switch ($ip)
{
	case "104.168.167.168" : 
//echo "live<br>";
	require "dataBaseConnect.web.php";
	break;

	case "173.17.96.138" : 
//echo "not live<br>";
	require "dataBaseConnect.local.php";
	break;

	case "192.168.1.20" : 
//echo "Home Server<br>";
	require "dataBaseConnect.local.php";
	break;

	case "::1" : 
//echo "localhost<br>";
	require "dataBaseConnect.local.php";
	break;

	default : 
//echo "There is no match";
	break;
}


if(isset($_POST['update'])){
	echo "POST update is True<br>";
	echo $_POST['student_id']."<br>";
	echo $_POST['student_name']."<br>";
	$updateSQL = "UPDATE wdv341_student SET student_name='$_POST[student_name]', student_address='$_POST[student_address]', student_email='$_POST[student_email]' WHERE student_id=$_POST[student_id]";
	//$updateSQL = "UPDATE wdv341_student SET student_name='ff', student_address='ff', student_email='ff' WHERE ID='1'";
	$record = $con->query($updateSQL)or die("Program Killed on UPDATE");
}

if(isset($_POST['delete'])){
	echo "POST delete is True<br>";
	$deleteSQL = "DELETE FROM wdv341_student WHERE student_id='$_POST[student_id]'";
	$record = $con->query($deleteSQL)or die("Program Killed on DELETE");
}

$sql = "SELECT * FROM wdv341_student";
$record = $con->query($sql)or die("Program Killed SELECTING ALL RECORDS");
echo $record->num_rows."<br>";
$myTable = "";
if ($record->num_rows > 0) {
		// output data of each row
	$i = 0;
	while($row = $record->fetch_assoc()) {
		//$myData[$i] = $myRowData = array('ID' => $row['student_id'], 'NAME' => $row['student_name'], 'ADDRESS' => $row['student_address'], 'EMAIL' => $row['student_email'] );
		$myData[$i] =  array('ID' => $row['student_id'], 'NAME' => $row['student_name'], 'ADDRESS' => $row['student_address'], 'EMAIL' => $row['student_email'] );
		echo $myData[$i]['NAME']. "<br>";

		$myTable .= "<form action=selectEventsModify.php method=post>";
		
		$myTable .= "<tr>";
		$myTable .= "<td>" . "<input type='text' name='student_id' value='" . $myData[$i]['ID'] . "' size=4' style='text-align:center;' readonly></td>";
		$myTable .= "<td>". "<input type='text' name='student_name' value='" .$myData[$i]['NAME']."'></td>";
		$myTable .= "<td>". "<input type='text' name='student_address' value='" .$myData[$i]['ADDRESS']."'></td>";
		$myTable .= "<td>". "<input type='text' name='student_email' value='" .$myData[$i]['EMAIL']."'></td>";
		$myTable .= "<td>" . "<input type='submit' name='update' value='update'" . " ></td>";
		$myTable .= "<td>" . "<input type='submit' name='delete' value='delete'" . " ></td>";
		$myTable .= "</tr>";

		$myTable .= "</form>";
	} // end while
 } // end if


?>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>WDV341 Intro PHP - Form Validation Example</title>
  <link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
  <style>

    #orderArea  {
      width:900px;
      background-color:#CF9;
    }

    .error  {
      color:red;
      font-style:italic;  
    }
    table {
      width: auto;
    }
    th#name {
    	width: 100px;
    }

  </style>
</head>
<body>
<div id="container">
<a href="selectEventsModify.php">Vist page again</a> 
<h1> This is a heading</h1>
	<table>
		<tr>
			<th id="id">ID</th>
			<th id="name">NAME</th>
			<th>ADDRESS</th>
			<th>EMAIL</th>
			<th>UPDATE</th>
			<th>DELETE</th>
		</tr>
		
		<?php echo $myTable; ?>
		
		
	</table>

<!-- Used to go back one page or return to root folder -->
<footer>
  <a href='eventsForm.php'>Visit Page Again</a><br>
  <a href='#' onClick='history.go(-1);return true;'>Go Back</a><br>
  <a href='wdv341.php'>Main Homework Page</a><br>
  <a href='./'>Return to Root Folder</a><br>
</footer>
</div> <!-- end of container -->
</body>
</html>