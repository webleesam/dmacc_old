<?php
	$tableData = "";	
	
	foreach($_POST as $key => $value)		
	{
		$tableData .= "<tr>";				
		$tableData .= "<td>$key</td>";		
		$tableData .= "<td>$value</td>";	
		$tableData .= "</tr>";				
	} 
 
	
	$firstName = $_POST["firstName"];	
	$lastName = $_POST["lastName"];
	$school = $_POST["school"];
	$age = $_POST["age"];
	$moreInfo = $_POST["moreInfo"];
	$mailingList = $_POST["mailingList"];
	$selectChoice = $_POST["selectChoice"];
	

?>
<!DOCTYPE html>
<html>
<head>
<title>Assignment: Form Handler Proccessing</title>
<link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
</head>

<body>
<h1>This assignment name is Form Handler Proccessing</h1>
	<table>
    <tr>
    	<th>Field Name</th>
        <th>Value of Field</th>
    </tr>
	<?php echo $tableData;  ?>
	</table>
</p>
<h4>Display the values </h4>
<p>First Name: <?php echo $firstName; ?></p>
<p>Last Name: <?php echo $lastName; ?></p>
<p>School: <?php echo $school; ?></p>
<p>Age: <?php echo $age; ?></p>
<p>Request more info: <?php echo $moreInfo; ?></p>
<p>Add to mailing list: <?php echo $mailingList; ?></p>
<p>Drop down selection: <?php echo $selectChoice; ?></p>

<footer>
	<p>Click<a href="#" onClick="history.go(-1);return true;">Here</a> to go back one page</p>
	<p>Click <a href="./wdv341.php">Here</a> to return to the PHP Homework Page</p></footer>
</body>
</html>
