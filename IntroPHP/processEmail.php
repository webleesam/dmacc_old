<?php require "OppMail.class.php" ?>
<?php
	$newEmail = new Email();

	$newEmail->setFromAddress('sales@webleesam.com');
	$newEmail->setToAddress('info@webleesam.com');
	$newEmail->setSubject('OPP EMAIL');
	$newEmail->setMessage('Here is the body of the message sent from my OPP Mail function');

	$myFrom = $newEmail->getFromAddress();
	$myTo = $newEmail->getToAddress();
	$mySubject = $newEmail->getSubject();
	$myMessage = $newEmail->getMessage();

	$mySentStatus = $newEmail->SendEmail($myTo,$mySubject,$myMessage,$myFrom);

 ?>
 <!DOCTYPE html>
<html>
<head>
<title>PROGRAMMING PROJECT: Email Class</title>
<link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
</head>

<body>
  <h1>This assignment name is Email Class</h1>

  <table>
  	<tr>
  		<td><?php echo $myFrom;  ?></td>
  	</tr>

  	<tr>
  		<td><?php echo $myTo;  ?></td>
  	</tr>

  	<tr>
  		<td><?php echo $mySubject;  ?></td>
  	</tr>

  	<tr>
  		<td><?php echo $myMessage;  ?></td>
  	</tr>

  	<tr>
  		<td><?php echo $mySentStatus;  ?></td>
  	</tr>

  </table>  

<footer>Click <a href="./wdv341.php">Here</a> to return to the PHP Homework Page</footer>
</body>
</html>