<?php
	$tableData = "";
	
	foreach($_POST as $key => $value)		
	{

		$tableData .= "<tr>";				
		$tableData .= "<td>$key</td>";		
		$tableData .= "<td>$value</td>";	
		$tableData .= "</tr>";				
	} 

	$date = $_POST['date'];
	$date = date_create($date);
	$date = date_format($date,"m/d/Y");

	$intDate = date_create($date);
	$intDate = date_format($intDate,"d/m/Y");

	$stringInput = $_POST['stringInput'];
	$stringInputLength = strlen($stringInput);
	$stringInputTrimmed = trim($stringInput);
	$stringInputTrimmedLength = strlen($stringInputTrimmed);
	$stringToLowerCase = strtolower($stringInputTrimmed);

	if (strpos($stringInput, 'DMACC') !== false) {
		$stringCheck_DMACC = "Yes!!!";
	} else
	{ 
		$stringCheck_DMACC = "No!!!";
	}

	$numberInput = $_POST['numberInput'];
	$moneyInput = $_POST['moneyInput'];

	$numberInput = number_format($numberInput);
	setlocale(LC_MONETARY, 'en_US.UTF-8');
	$moneyInput = money_format('%.2n',$moneyInput);


?>
<!DOCTYPE html>
<html>
<head>
<title>Assignment: PHP Functions Proccessing</title>
<link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
</head>

<body>

<h1>This assignment name is PHP Functions Proccessing</h1>
	<table>
    <tr>
    	<th>Field Name</th>
        <th>Value of Field</th>
    </tr>
	<?php echo $tableData;  ?>
	</table>



<h4>Display the reformatted values </h4>
<p>Create a function that will accept a date input and format it into mm/dd/yyyy format.</p>
<p>Date: <?php echo $date; ?></p>
<br>
<p>Create a function that will accept a date input and format it into dd/mm/yyyy format to use when working with international dates.</p>
<p>Date: <?php echo $intDate; ?></p>
<br>
<p>Create a function that will accept a string input. Display the number of characters in the string, Trim any leading or trailing whitespace, Display the string as all lowercase characters, Will display whether or not the string contains "DMACC" either upper or lowercase</p>
<p>Number of characters: <?php echo $stringInputLength; ?></p>
<p>Trim leading and trailing whitespace: <?php echo $stringInputTrimmed; ?></p>
<p>Number of characters in trimmed string: <?php echo $stringInputTrimmedLength; ?></p>
<p>display in lower case: <?php echo $stringToLowerCase; ?></p>
<p>Does it contain DMACC: <?php echo $stringCheck_DMACC; ?></p>
<br>
<p>Create a function that will accept a number and display it as a formatted number. Use 1234567890 for your testing.</p>
<p>Date: <?php echo $numberInput; ?></p>
<br>
<p>Create a function that will accept a number and display it as US currency. Use 123456 for your testing.</p>
<p>Date: <?php echo $moneyInput; ?></p>
<br>


<footer>
	<p>Click<a href="#" onClick="history.go(-1);return true;">Here</a> to go back one page</p>
	<p>Click <a href="./wdv341.php">Here</a> to return to the PHP Homework Page</p></footer>
</body>
</html>
