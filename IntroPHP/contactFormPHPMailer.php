<?php
/*echo '<script language="javascript">';
echo "alert('PHP Loaded')";
echo '</script>';*/




?><!-- end of php -->

<!DOCTYPE HTML>
<html>
<head>
  <title>PROGRAMMING PROJECT: Contact Form with Email</title>
  <link rel="stylesheet" type="text/css" href="css/projectPageStyle.css">
</head>

<body>
  <div id="container">
    <h1>PROGRAMMING PROJECT: Contact Form with Email</h1>
    <h2>using PHP Mailer</h2>

    <form id="emailForm" name="emailForm" method="post" action="processFormPHPMailer.php">
    <table>
    <tr>
    <td>Your Name:</td>
    <td><input type="text" name="yourName" id="yourName"></td>
    </tr>
      

    <tr>
    <td>Your Email:</td>
    <td><input type="text" name="yourEmail" id="yourEmail"></td>

    <tr>
    <td>Reason for contact:</td>
        <td>
        <select name="selectChoice" id="selectChoice">
          <option value="default">Please Select a Reason</option>
          <option value="product">Product Problem</option>
          <option value="return">Return a Product</option>
          <option value="billing">Billing Question</option>
          <option value="technical">Report a Website Problem</option>
          <option value="other">Other</option>
        </select>
        </td>
      

      </table>  

      <p>Comments:</p>
      <textarea name="yourComments" id="yourComments" cols="45" rows="5"></textarea>

      
      <p><input type="checkbox" name="mailingList" id="mailingList" checked>Please put me on your mailing list.</p>

      <p><input type="checkbox" name="moreInfo" id="moreInfo" checked><label>Send me more information about your products.</label></p>



      <input type="submit" name="submit" id="submit" value="Submit">
      <input type="reset" name="reset" id="reset" value="Reset">
    </p>
  </form>
</div><!-- end of container -->
<footer>Click <a href="./wdv341.php">Here</a> to return to the PHP Homework Page</footer>
</body>
</html>
