<?php
session_start();


if ($_SESSION['validUser'] == "yes") {  
  echo $_SESSION['validUser']."Welcome Back! $name<br>";  
} else {
  echo $_SESSION['validUser']."You need to log on $name <br>"; 
  $name = "";
  $nameError="";
  $password = "";
  $passwordError="";

  // 2nd set form valid to false. assume form is false, let validation functions change it to true
  $validForm=false;

  // 3rd create validattion functions. must pass (define) variables globally
  function validateBlank($x) {
    global $validForm;
    $xError = ""; 
    if(empty($x)) {
      $validForm = false;         
      $xError .= "Input Field cannot be blank!"; 
      return $xError; 
    } 
  }

  function validateSpecial($x) {
    global $validForm;
    $xError = "";
    if (preg_match("([\'\/~`\!@#\$\"\\%\^&\*\(\)_\-\+=\{\}\[\]\|;:<\>,\.\?])",$x)) {  //From http://stackoverflow.com/questions/3937569/preg-match-php-special-characters
      $validForm = false;
      $xError .= "Input Field contains an illegal character!"; 
      return $xError;
    }
  }

  function validateName ($x) {
    global $nameError;
    $nameError .= validateBlank($x);
    $nameError .= validateSpecial($x);
  }

  function validatePassword ($x) {
    global $passwordError;
    $passwordError .= validateBlank($x);
    $passwordError .= validateSpecial($x);
  }

  if (isset($_POST["submit"])) {
    echo "Form has been submitted<br>";
    $name = ($_POST["name"]);
    $password = ($_POST["password"]);
    // 2nd set form valid to true. assume form is true, let validation functions change it to false
    $validForm=true;
    validateName($name);
    validatePassword($password);
	  if ($validForm==true) {
      // connect to database when form is valid - check if local or live (on WebLeeSam.com)
  		$ip = $_SERVER['SERVER_ADDR'];
  		echo $ip . "<br>";
  		switch ($ip) {
  			case "104.168.167.168" : 
  			echo "live<br>";
  			require "dataBaseConnect.web.php";
  			break;

  			case "173.17.96.138" : 
  			echo "not live<br>";
  			require "dataBaseConnect.local.php";
  			break;

  			case "192.168.1.20" : 
  			echo "Home Server<br>";
  			require "dataBaseConnect.local.php";
  			break;

  			case "::1" : 
  			echo "localhost<br>";
  			require "dataBaseConnect.local.php";
  			break;

  			default : 
  			echo "There is no match";
  			break;
		  }
      echo "create statement next<br>";
		  $statement = "SELECT event_user_user,event_user_password FROM event_user WHERE event_user_user = ? AND event_user_password = ?";        
		  $query = $con->prepare($statement) or die("Program Killed 01");  
		  //printf("Error: %s.\n", $stmt->error);
		  $query->bind_param("ss",$name,$password)  or die("Program Killed 02"); 
		  $query->execute()  or die("Program Killed 03"); 
		  $query->bind_result($name,$password) or die("Program Killed 04"); ;
		  $query->store_result();
		  $query->fetch();
      echo $query->num_rows."<br>";
		  //If this is a valid user there should be ONE row only
		  if ($query->num_rows == 1 ) {
		    $_SESSION['validUser'] = "yes";       //this is a valid user so set your SESSION variable
        echo "Welcome Back! $userName <br>";
		    $message = "Welcome Back! $userName";
		    //Valid User can do the following things:
		  } else {
		  	//error in processing login.  Logon Not Found...
		  	$_SESSION['validUser'] = "no";          
        echo "Sorry, there was a problem with your username or password. Please try again.<br>";
		  	$message = "Sorry, there was a problem with your username or password. Please try again.";
		  }
		  $query->close();
		  $con->close();
    } else {
      echo "form is not valid<br>";
	  }	  
	} else {
		echo "Form has NOT been submitted<br>";
  }
  ?>
	<!DOCTYPE html>
	<html>
	<head>
	<title>logOn.php</title>
	<link rel="stylesheet" type="text/css" href="css/projectPageStyle.css">
	<link rel="stylesheet" type="text/css" href="css/eventsStyle.css">
	<style type="text/css">
	  label, input, span { margin: 5px; }
	  .error  { color:red; font-style:italic; }
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script>
	  $(document).ready(function(){
  		$("#clearForm").click(function(){
  		  $("#Name").val("");
  		  $("#Password").val("");
  		});
	  });
	</script>
	</head>
	<body>
	<div id="container">
    <h2><?php echo $message?></h2>
    <?php
    if ($_SESSION['validUser'] == "yes") {
      ?>
      <h3>Presenters Administrator Options</h3>
      <p><a href="timesheetEntry.php">Input Timesheet Data</a></p>
      <p><a href="timesheetResults.php">Review Timesheet Data</a></p>
      <p><a href="logOut.php">Log Off</a></p> 
      <?php
    } else {
      ?>
  	  <h1>Log On Form</h1>
  	  <form id="myForm" name="myForm" method="post" action="logOn.php">
  		<div>
  		  <label>Name: </label>
  		  <input type="text" id="name" name="name" value="<?php echo $name ?>"><span class="error"><?php echo $nameError ?>
  		</div>
  		<div>
  		  <label>Password: </label>
  		  <input type="text" id="password" name="password" value="<?php echo $password?>"><span class="error"><?php echo $passwordError ?>
  		</div>
  		<input type="submit" name="submit" id="submit" value="Submit">
  		<input type="reset" name="reset" id="reset" value="Reset">
  		<input type="button" name="clearForm" id="clearForm" value="Clear Form">
  	  </form>
      <?php
    } // End of Else for Valid User in view
} // End of Else for Valid User in controller
?>
    <!-- Used to go back one page or return to root folder -->
  <footer>
    <a href='logOn.php'>Visit Page Again</a><br>
    <a href='#' onClick='history.go(-1);return true;'>Go Back</a><br>
    <a href='../wdv341.php'>Main Homework Page</a><br>
    <a href='../'>Return to Root Folder</a><br>
  </footer>

  </div> <!-- end of container -->
  </body>
</html>