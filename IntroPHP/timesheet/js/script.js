// alert("script.js loaded");
function Clear() {    
   document.getElementById("name").value= "";
   $('#suggest').hide();
}

function ClearName() {    
   document.getElementById("jobname").value= "";
}

function ClearNumber() {    
   document.getElementById("jobnumber").value= "";
}


function autoComplete() {
	var min_length = 0;                      // min caracters to display the autocomplete
	var keyword = $('#name').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'autoCompleteInput.php',
			type: 'POST',
			data: {keyword:keyword},     //keyword is being posted to the php, keywordstate is the varible from javascript
			success:function(data){
				$('#suggest').show();
				$('#suggest').html(data);
				$('#suggest ul li').mouseover(function(){
					$('#suggest ul li').removeClass("hoover");
					$(this).addClass("hover");
				})
				$('#suggest ul li').click(function(){
					var value = $(this).html();
					$("#name").val(value);
					$('#suggest').hide();
				})
			}
		});
	} else {
		$('#suggest').hide();
	}
}

