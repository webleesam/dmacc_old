<?php
session_start();
if ($_SESSION['validUser'] == "yes") {
	echo $_SESSION['validUser']."Welcome Back! $name<br>";  
	include_once ('func.inc.php');
	connect ();
	?>	
	<!DOCTYPE html>
	<html>
	<head>
    <title>Time Sheet Entry Form</title>
    <!-- <link rel="stylesheet" href="style/style.css"> -->
    <script src="jquery-3.1.1.min.js"></script>
	<script>
		$(document).ready(function(){
			$('body').on('focus',".datepicker", function(){
				$(this).datepicker();
			})
		});	
	</script>
	<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
		<div id="container">
			<div id="myForm">
				<form id="entryForm" action="timesheet.php" method="post">
					<div id="nameBox">
						<p>Name: <input id="name" name="name" onClick="Clear();" onkeyup="autoComplete()" autocomplete="off"></input></p>
						<div id="suggest"></div>	
					</div>
					<div id="entry" class="row">
						<div class="ticket"><p>Date:<br /><input type="text" class="datepicker" name="date" size="15" value="Click here to select date"></p></div>
						<div class="ticket"><p>Job Number: <br /><input type="text" name="jobnumber" onClick="ClearNumber();" id="jobnumber" size="20" value="Enter in a Job Number" /></p></div>
						<div class="ticket"><p>Job Name: <br /><input type="text" name="jobname" onClick="ClearName();" id="jobname" size="20" value="Enter in a job name" /></p></div>
						<div class="ticket"><p>RT: <br /><input type="text" name="rt" size="3" value=0 /></p></div>
						<div class="ticket"><p>OT: <br /><input type="text" name="ot" size="3" value=0 /></p></div>
						<div class="ticket"><p>DT: <br /><input type="text" name="dt" size="3" value=0 /></p></div>
					</div>
				<input type="submit" value="Submit" />
				<input type="reset" value="Reset" />
				</form>
			</div> <!-- end of myForm -->
			<hr>       <!--    Retrieve            -->
			<form action="printTime.php" method="post">
				<p>Name: <input type="text" id="name" name="name"></p>
				<p>Job Name: <input type="text" name="jobname"></p>
				<p>Job Number: <input type="text" name="jobnumber"></p>
				<p>RT: <input type="text" name="rt"></p>
				<p>OT: <input type="text" name="ot"></p>
				<p>DT: <input type="text" name="dt"></p>
				<p>Start Date: <input type="text" class="datepicker" name="startDate" size="15" value="Click here to select date"></p>
				<p>End Date: <input type="text" class="datepicker" name="endDate" size="15" value="Click here to select date"></p>
				<p></p>
				<input type="submit" value="Submit" />
				<input type="reset" value="Reset" />		
			</form></br>
			<?php display() ?>
			<?php close() ?>	
		</div> <!-- end of container -->
	<?php
} else {

echo "Sorry, there was a problem with your username or password. Please try again.<br>";
echo $_SESSION['validUser']."You need to log on $name <br>";
header('Location: logOn.php');
}
?>		
	</body>
	</html> 
