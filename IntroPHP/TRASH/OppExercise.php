<?php
	class Product
	{
		//Comments -- Created Sept 20,2016
		//Define Properties
		public $productName; // define property name
		
		public $productQuantity; // the number of product avaiable
		
		
		//Define Methods -setters
		function setProductName($inProductName){
			$this->productName = $inProductName; // setter method
		}
		
		function setProductQuantity($inProductQuantity){
			$this->productQuantity = $inProductQuantity; // setter method
		}
		
		//Define Methods - getters
		function getProductName(){
			return $this->productName;
		}
		
		function getProductQuantity(){
			return $this->productQuantity;
		}

		//Define Methods - Processing Methods - To change Quantity
		function decreaseQuantity($inNewQty)
		{
			$totalQty -= $inNewQty; //Subtract incoming quantity to total
		}

		function increaseQuantity($inNewQty)
		{
			$totalQty += $inNewQty; //Add incoming quantity to total
		}

			
	}// end of class Product

?>