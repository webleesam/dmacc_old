<!DOCTYPE html>
<html>
	<head>
		<title>Assignment: Research PDO</title>
		<style>

		</style>
		<link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script>
			$(document).ready(function(){

				
			});

		
		</script>
		
	</head>
	
	<body >
	<div id="container">
		<h1>Assignment: Research PDO</h1>
		<h2 id="returnHome"><a href="javascript:history.back()">Click Here to Return to Homework Page</a></h2>
	
		<h3>Research PDO</h3>
		
		<ol>
			<li>What does PDO mean?</li>
				<ul>
					<li>"PDO stands for PHP Data Object"-- Credit--<a href="http://php.net/manual/en/intro.pdo.php">http://php.net/manual/en/intro.pdo.php</a></li>
				</ul>
			<li>What does PDO do?</li>
				<ul>
					<li>"Used to communicate with databases. It is unique in that it does not matter what type of database you are communicating with, unlike mySQLi commands that are designed to talk to a mySQL database"-- Credit--<a href="http://php.net/manual/en/intro.pdo.php">http://php.net/manual/en/intro.pdo.php</a></li>
				</ul>
			<li>What are some of the advantages of using PDO?</li>
				<ul>
					<li>"Use of prepared statements, multiple database support"-- Credit--<a href="https://www.quora.com/What-are-advantages-of-PDO-in-PHP">https://www.quora.com/What-are-advantages-of-PDO-in-PHP</a></li>
				</ul>
			<li>What are some issues you need to be aware of when using PDO?</li>
				<ul>
					<li>"Not completely sure. Did not find any issues except maybe this on: PDO substitutes placeholders with actual data, instead of sending it separately. And with "lazy" binding (using array in execute()), PDO treats every parameter as a string. As a result, the prepared LIMIT ?,? query becomes LIMIT '10', '10' which is invalid syntax that causes query to fail."-- Credit--<a href="https://phpdelusions.net/pdo#limit">https://phpdelusions.net/pdo#limit</a></li>
				</ul>
			<li>What do you need to do to include PDO in your PHP pages?</li>
				<ul>
					<li>"PDO is Object Orintated, so you need a 'new' instance"-- Credit--<a href="http://php.net/manual/en/pdo.connections.php">http://php.net/manual/en/pdo.connections.php</a></li>
				</ul>
		</ol>
	</div> <!--End of container-->
	<footer>Click <a href="./wdv341.php">Here</a> to return to the PHP Homework Page</footer>	
	</body>
</html> 