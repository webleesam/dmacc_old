<!DOCTYPE html>
<html>
	<head>
		<title>PHP Homework Page</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="css/homeworkPageStyle.css">
		<link rel="stylesheet" type="text/css" href="css/addOnStyle.css">
	</head>
	
	<body>
		<div class="container">
		<div class="jumbotron text-center">
			<h1>This is my Homework page for Intro to PHP</h1>
			<h2 id="blankSpace"></h2>
			<h2 id="myName">
				<ol>Brian Coffie</ol>
				<ol>515.989.1298</ol>
				<ol><a href="http://webleesam.com">WebLeeSam.com</a></ol>
				<h3>Here are the links to my assingment projects</h3>
				<h4>Unit 1 PHP Basics</h4>

			</h2>
			<div class="container-brc">
				<div class="row">
					<div class="col-sm-4">
						<p id="PHP_Basics"><a href="PHP_Basics.php">Assignment: PHP Basics</a></p>
						<sup>(PHP_Basics.php)</sup><br>
					</div>
					<div class="col-sm-4">
						<p id="basicFormHandler"><a href="basicFormHandler.html">Assignment: Form Handler</a></p>
						<sup>(basicFormHandler.html)</sup><br>
						<sup>(basicFormHandlerProcessing.php )</sup><br>
					</div>
					<div class="col-sm-4">
						<p id="FormEmailer"><a href="formEmailer.html">Assignment: Form Emailer</a></p>
						<sup>(FormEmailer.html)</sup><br>
						<sup>(FormEmailerProcessing.php )</sup><br>
						<sup>(contactFormPHPMailer.php )</sup><br>
					</div>
					<div class="col-sm-4">
						<p id="phpFunctions"><a href="phpFunctions.php">Assignment: PHP Functions</a></p>
						<sup>(phpFunctions.php)</sup><br>
						<sub>(phpFunctionsProcessing.php)</sub><br>
					</div>
					<div class="col-sm-4">
						<p id="contactForm"><a href="contactForm.php">PROGRAMMING PROJECT: Contact Form with Email</a></p>
						<sup>(contactForm.php)</sup><br>
						<sub>(processForm.php)</sub><br>
					</div>
					<div class="col-sm-4">
						<p><a href="processEmail.php">PROGRAMMING PROJECT: Email Class</a></p>
						<sup>(processEmail.php)</sup><br>
						<sub>(OppMail.class.php)</sub><br>
					</div>	<!-- PROGRESS -->
					<div class="col-sm-4">
						<p><a href="gitReview.php">Assignment: Git Terminology</a></p>
						<sup>(gitReview.php)</sup><br>
						<sub>(#)</sub><br>
					</div>
					<div class="col-sm-4">
						<p><a href="#">Assignment: Download Git Client and Create a Bitbucket account</a></p>
						<sup>(#)</sup><br>
						<sub>(#)</sub><br>
					</div>
					<div class="col-sm-4">
						<p><a href="formValidationAssignment.php">Assignment: Validate Form</a></p>
						<sup>(formValidationAssignment.php)</sup><br>
						<sub>(#)</sub><br>
					</div>
					<div class="col-sm-4">
						<p><a href="contactFormValidationProject.php">Programming Project: PHP Contact Page with Validation</a></p>
						<sup>(contactFormValidationProject.php)</sup><br>
						<sub>(#)</sub><br>
					</div>
					<div class="col-sm-4">
						<p><a href="Database Capture.JPG">Assignment: MySQL Create Database and tables</a></p>
						<sup>(Database Capture.JPG)</sup><br>
					</div>
					<div class="col-sm-4">
						<p><a href="dataBaseConnect.local.php">Assignment: Create a connection file(local)</a></p>
						<sup>(dataBaseConnect.local.php)</sup>
						<p><a href="dataBaseConnect.web.php">Assignment: Create a connection file(web)</a></p>
						<sup>(dataBaseConnect.web.php)</sup>
					</div>
					<div class="col-sm-4">
						<p><a href="researchPDO.php">Assignment: Research PDO</a></p>
						<sup>(researchPDO.php)</sup><br>
						<sub>(#)</sub><br>
					</div>
					<div class="col-sm-4">
						<p><a href="eventsForm.php">Assignment: Create a form page for the events</a></p>
						<sup>(eventsForm.php)</sup>
					</div>
					<div class="col-sm-4">
						<p><a href="eventsForm.php">Programming Project: Contact Form Adding to Database</a></p>
						<sup>(eventsForm.php)</sup>
					</div>
					<div class="col-sm-4">
						<p><a href="selectEvents.php">Assignment: Create selectEvents.php</a></p>
						<sup>(selectEvents.php)</sup><br>
					</div>
					<div class="col-sm-4">
						<p><a href="selectEventsOne.php">Assignment: Create selectOneEvent.php</a></p>
						<sup>(selectEventsOne.php)</sup><br>
					</div>
					<div class="col-sm-4">
						<p><a href="selectEventsModify.php">Assignment: Modify selectEvents.php</a></p>
						<sup>(selectEventsModify.php)</sup><br>
					</div>
					<div class="col-sm-4">
						<p><a href="selectEventsModify.php">Assignment: Create deleteEvent.php page</a></p>
						<sup>(selectEventsModify.php)</sup><br>
					</div>
					<div class="col-sm-4">
						<p><a href="selectEventsModify.php">Assignment: Create the updateEventForm.php page</a></p>
						<sup>(selectEventsModify.php)</sup><br>
					</div>
					<div class="col-sm-4">
						<p><a href="Logon Database Capture.JPG">Assignment: Create event_user table</a></p>
						<sup>(Logon Database Capture.JPG)</sup><br>
					</div>
					<div class="col-sm-4">
						<p><a href="logOn.php">Assignment: Create a login.php page</a></p>
						<sup>(logOn.php)</sup><br>
					</div>
					<div class="col-sm-4">
						<p><a href="logOut.php">Assignment: Create a logout.php page</a></p>
						<sup>(logOut.php)</sup><br>
					</div>
					<div class="col-sm-4">
						<p><a href="#">Assignment: Protect your dynamic pages</a></p>
						<sup>(#)</sup><br>
						<sub>(#)</sub><br>
					</div>
					<div class="col-sm-4">
						<p><a href="timesheet/timesheetEntry.php">Final Project -- My Timesheet</a></p>
						<sup>(timesheet/timesheetEntry.php)</sup><br>
					</div>

				</div>


			</div> <!-- end of container (brc container) -->
			
			




		</div><!-- end of bootstrap jumbotron -->
		</div> <!-- end of container (bootstrap container) -->
		<footer>Click <a href="./" >Here</a> to go to directory listing</footer>
	</body>
</html> 