<?php

?>
<!DOCTYPE html>
<html>
<head>
<title>Assignment: PHP Functions</title>
<link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
<?php echo $myArray; ?>
<style type="text/css">
	input[type=date]::-webkit-inner-spin-button {
    -webkit-appearance: none;
    display: none;
}
</style>
</head>
<body>
	
	<h1>This assignment name is PHP Functions</h1>

	<form method="post" action="phpFunctionsProcessing.php">
	<p>Create a function that will accept a date input and format it into mm/dd/yyyy and dd/mm/yyyy format</p>
		Date:
		<input type="date" name="date"><br>


		<p>Create a function that will accept a string input. Display the number of characters in the string, Trim any leading or trailing whitespace, Display the string as all lowercase characters, Will display whether or not the string contains "DMACC" either upper or lowercase</p>
		String Input:
		<input type="input" name="stringInput"><br>

		<p>Create a function that will accept a number and display it as a formatted number. Use 1234567890 for your testing.</p>
		Number Input:
		<input type="input" name="numberInput" value="1234567890"><br>

		<p>Create a function that will accept a number and display it as US currency.  Use 123456 for your testing.</p>
		Number Input:
		<input type="input" name="moneyInput" value="123456"><br>

		<br>
		<input type="submit">
	</form>
	
	<footer>Click <a href="./wdv341.php">Here</a> to return to the PHP Homework Page</footer>

</body>
</html> 