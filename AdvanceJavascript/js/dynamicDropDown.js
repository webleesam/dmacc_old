var productsArray = new Array("Pen","Paper","Pencil");
var colorsArray = new Array("Red","Yellow","Green");

function changeDropDownOptions(radioChoice) {
	productSelection = radioChoice;
}

$(document).ready(function(){
	$('input:radio[name=radio]').change (function () {
		myVal = $(this).val();
		$("#displayItems option").remove();
		if (myVal == "addProducts") {
			for(i=0; i<productsArray.length; i++) {
				$("#displayItems").append("<option>" + productsArray[i] + "</option>");
			}
		}
		if (myVal == "addColors") {
			for(i=0; i<productsArray.length; i++) {
				$("#displayItems").append("<option>" + colorsArray[i] + "</option>");
			}
		}
	})
});	

function showArray() {
	for(i=0; i<productsArray.length; i++)
	{
		document.write("<option>" + productsArray[i] + "</option>" );
	} 	
}