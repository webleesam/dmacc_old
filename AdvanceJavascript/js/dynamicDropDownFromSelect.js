var complaintsArray = new Array("Too Hot","Too Cold","Too Big","Too Small");

$(document).ready(function(){
	$("#relatedProblem").css("display","none")
	$("#techArray").css("display","none")
	$("#issueArray").css("display","none")

	$('select#contactType').change (function () {
		myVal = $(this).val();
		$("#relatedProblem").show();
		$("#techArray").css("display","none")
		$("#issueArray").css("display","none")
		$("#contactType option[value='X']").remove();
		if (myVal == "C") {
			for(i=0; i<complaintsArray.length; i++) {
				$("#relatedProblem").append("<option>" + complaintsArray[i] + "</option>");
			}
		}
		if (myVal == "T") {
			$("#techArray").show();
			$("#relatedProblem").css("display","none")
			$("#issueArray").css("display","none")
		}
		if (myVal == "P") {
			$("#issueArray").show();
			$("#techArray").css("display","none")
			$("#relatedProblem").css("display","none")
		}

	})
});