
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>AJAX Team Project</title>
<style type="text/css">
  span {
    color:red;
  }

</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<script>
// Wait 8000 miliseconds and then run this AJAX call function to change the options to candy pulled from the XML document
  setTimeout (function(){
  desc = [];  // create global arrays
  price = [];
  img = [];
  $.ajax({
    type: "GET",

    url: "candy.xml",

    cache: false,

    dataType: "xml",

    success: function(xml) {
      $("#select option").remove(); //remove the cupcake options
      $('#select').append($("<option></option>").attr("value","Please make Selection").text("Please make Selection"));  // append the please make selection option to the now empty select

      $(xml).find('candy').each(function(){
        $(this).find("name").each(function(){
          var name = [];  // create array
          name.push($(this).text()); // push name data into array from AJAX call while looping through each name in the xml using the each command
          for (i = 0; i < name.length; i++) {
             $('#select').append($("<option></option>").attr("value",name[i]).text(name[i])); // build the option choices by appending to the select and using a for loop to cycle through the name array
          }
        });
      });

      $(xml).find('candy').each(function(){
        $(this).find("price").each(function(){
          
          price.push($(this).text()); // push price data into array from AJAX call while looping through each price in the xml using the each command
        });
      });

      $(xml).find('candy').each(function(){
        
        $(this).find("desc").each(function(){
          
          desc.push($(this).text());  // push desc data into array from AJAX call while looping through each desc in the xml using the each command
        });
      });

      $(xml).find('candy').each(function(){

        $(this).find("img").each(function(){

          img.push($(this).text()); // push img data into array from AJAX call while looping through each img in the xml using the each command
        });
      });
    },

    error: function(xhr, textStatus, errorThrown){
      alert('request failed '+errorThrown); // alert error if AJAX call fails
    }

  });


  },8000); // end of setTimeOut

</script>

<script type="text/javascript">
// Document Ready - 
$(document).ready(function(){
  $('select#select').change (function () {
    $("#select option[value='Please make Selection']").remove(); // watch for select to change and remove please make selection

    myVal = $(this).val(); // set variable to selection choice

    if (myVal == "Twix") {  // evaluate selection
      x=0;
      $("#desc").html(desc[x]); // populate span with id of desc with data from array
      $("#price").html(price[x]); // populate span with id of price with data from array
      $("#img").html(img[x]); // populate span with id of img with data from array
      $("#imgPic").attr("src", "img/"+img[x]); // change attribute src of image with imgPic tag with data from array
    }

    if (myVal == "KitKat") {  //same as above
      x=1;
      $("#desc").html(desc[x]);
      $("#price").html(price[x]);
      $("#img").html(img[x]);
      $("#imgPic").attr("src", "img/"+img[x]);

    }

    if (myVal == "Snickers") {  //same as above
      x=2;
      $("#desc").html(desc[x]);
      $("#price").html(price[x]);
      $("#img").html(img[x]);
      $("#imgPic").attr("src", "img/"+img[x]);
 
    }

    if (myVal == "Reeses") {  //same as above
      x=3
      $("#desc").html(desc[x]);
      $("#price").html(price[x]);
      $("#img").html(img[x]);
      $("#imgPic").attr("src", "img/"+img[x]);

    }

  })

}); // end of Document Ready
</script>

<link href="TeamProjectForm.css" rel="stylesheet" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Creepster" rel="stylesheet">

</head>

<body>
<h1>WDV321 Advanced Javascript</h1>
<h2>AJAX Team Project</h2>
<h3>Team Members: Brian Coffie, Holly Johnson, and </h3>
<p>&nbsp;</p>
<section id="container">
<form id"halloween" method="post" action="">
<h3>TRICK OR TREAT!!!</h3>
  <p>Make your choice: 
    <label>
      <select name="select" id="select">
        <option value="van">Vanilla Cupcake</option>
        <option value="choc">Chocolate Cupcake</option>
        <option value="red">Red Velvet Cupcake</option>
      </select>
    </label>
  </p>
  <p>Description:<span id="desc"></span></p>

  <p>Price:<span id="price"></span></p>

  <p>Image:<span id="img"></span></p>
  <img id="imgPic" src="" alt="" height="160" width="320">

  <p>
    <input type="submit" name="button" id="button" value="Submit">
    <input type="submit" name="button2" id="button2" value="Submit">
  </p>
</form>
</div>
<p class="bold">Extra Credit:</p>
<p>Use an Ajax call to dynamically change the select to a choice of candy instead of cupcakes. Include the appropriate server side information. </p>
</body>
</html>