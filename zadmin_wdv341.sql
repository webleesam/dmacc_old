-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 19, 2016 at 07:26 PM
-- Server version: 5.1.73
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `zadmin_wdv341`
--

-- --------------------------------------------------------

--
-- Table structure for table `wdv341_event`
--

CREATE TABLE IF NOT EXISTS `wdv341_event` (
  `event_id` int(12) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(85) NOT NULL,
  `event_description` varchar(85) NOT NULL,
  `event_presenter` varchar(85) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` time NOT NULL,
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `wdv341_event`
--

INSERT INTO `wdv341_event` (`event_id`, `event_name`, `event_description`, `event_presenter`, `event_date`, `event_time`) VALUES
(1, 'Dancing', 'Left foot Right foot event', 'Mr. Dance Fever', '2016-11-01', '12:00:00'),
(3, 'Singing', 'Lip movement exercise', 'Tone Def Jr.', '2016-12-10', '17:15:00'),
(4, 'Flu Shots', 'Vaccination for illness', 'Ben Dover', '2016-05-13', '08:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `wdv341_student`
--

CREATE TABLE IF NOT EXISTS `wdv341_student` (
  `student_id` int(3) NOT NULL AUTO_INCREMENT,
  `student_name` varchar(28) NOT NULL,
  `student_address` varchar(28) NOT NULL,
  `student_email` varchar(28) NOT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
